
public abstract class Empleado implements LegislacionLaboralChile{
	private String Identification;
	private String nombre;
	private String horasTrabajadas;
	private String Salario;
	
	public String getSalario() {
		return Salario;
	}

	public void setSalario(String salario) {
		Salario = salario;
	}

	public String getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(String horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public void setIdentification(String identification) {
		Identification = identification;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIdentification() {
		return Identification;
	}
	
	public void setIdentidication(String identification) {
		Identification = identification;
	}
	
	public abstract String trabajar(String trabajo);
}
