import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class PersonalAseo extends Empleado implements LegislacionLaboralChile{

	private String rondaLimpieza;
	
	PersonalAseo(){
		rondaLimpieza = "No se ha limpiado nada";
	}
	
	public String getRondaLimpieza() {
		return rondaLimpieza;
	}
	
	@Override
	public String trabajar(String lugar) {
		rondaLimpieza += " \n"+  lugar;
		return rondaLimpieza;
	}

	@Override
	public int calcularSalario(int dias) {
		int salario = dias*9600;
		this.setSalario(Integer.toString(salario));
		return salario;
	}

	public static String leerArchivo(){

	      File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;
	      String herramientas = "";
	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	         archivo = new File ("C:\\Users\\VIT_P2400\\Documents\\archivo.txt");
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);

	         // Lectura del fichero
	         String linea;
	         while((linea=br.readLine())!=null)
	            herramientas += linea;
	      }
	      catch(Exception e){
	         
	    	 return  " algo anda mal "+e.getMessage();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            return e2.getMessage();
	         }
	      }		
	      return herramientas;
	}
	
	public static void escribirArchivo(String texto) {
		FileWriter fichero = null;
        PrintWriter pw = null;
        String herramientas = leerArchivo();
        herramientas+=", "+texto;
        try
        {
            fichero = new FileWriter("C:\\Users\\VIT_P2400\\Documents\\archivo.txt");
            pw = new PrintWriter(fichero);

                pw.println(herramientas);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
	}
}
