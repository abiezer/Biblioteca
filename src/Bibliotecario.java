import java.util.ArrayList;

public class Bibliotecario extends Empleado implements LegislacionLaboralChile{
	
	private String librosPrestados;
	private ArrayList<Libro> libros = new ArrayList<>();
	
	public ArrayList<Libro> getLibros() {
		return libros;
	}

	public void setLibros(ArrayList<Libro> libros) {
		this.libros = libros;
	} 

	Bibliotecario(){
		librosPrestados = "No se ha Prestado Ningun Libro";
	}

	@Override
	public String trabajar(String libro) {
		librosPrestados += "\n"+libro;
		return librosPrestados;
	}

	@Override
	public int calcularSalario(int horas) {
		int salario = horas*1600;
		this.setSalario(Integer.toString(salario));
		return salario;
	}
	
	public int calcularSalario(int horas, int precioXHora) {
		int salario = horas*precioXHora;
		this.setSalario(Integer.toString(salario));
		return salario;
	}
	
	public void aniadirlibro(Libro librito) {
		this.libros.add(librito);
	}
	
	public static void didvidir(double dividendo,double divisor) {
		
	}

}
