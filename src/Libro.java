
public class Libro {
	private String titulo;
	private String anioPublicacion;
	
	
	/**
	 * El Metodo getTitulo me entrega el titulo de un objeto libro
	 * @return titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAnioPublicacion() {
		return anioPublicacion;
	}
	public void setAnioPublicacion(String anioPublicacion) {
		this.anioPublicacion = anioPublicacion;
	}
	
	
	
}
